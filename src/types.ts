export interface ExperienceModel {
  title: string,
  companyName: string,
  location: string,
  startDate: string,
  endDate: string,
  description: string
}
